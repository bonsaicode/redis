<?php
namespace BonsaiCode;
/**
 * BonsaiCode/Redis
 *
 * Implementation of the Redis Redlock algorithm, which is used to ensure only one server carries out a process in an environment where multiple duplicate servers exist, e.g. GCP auto-scaling load balancer.
 * Also provides a wrapper for some phpredis methods.  (phpredis is faster than predis)
 *
 * connect() creates redis object in $GLOBALS['REDIS']:
 *
 * see https://github.com/phpredis/phpredis#connection for more info
 *
 * @author Alan Davis <alan@bonsaicode.dev>
 * @copyright 2018 BonsaiCode, LLC
 *
 */

class Redis {

	/**
	 * connect - Connect to redis and select database.  Create redis object in $GLOBALS['REDIS'].  If connection cannot be made, $GLOBALS['REDIS'] is set to null.
	 *
	 * @param $options = [
	 * 			'host'    => '10.0.0.3',
	 * 			'port'    => 6379,
	 * 			'timeout' => 2, # timeout in seconds for connecting
	 * 			'auth'    => optional,  # if given, then it also needs to be in /etc/redis.conf and /etc/php.d/50-redis.ini - session.save_path
	 * 			'dbindex' => optional database index to select/use
	 * 		]
	 */
	static function connectSelect( array $options ) : void {

		try {
			$GLOBALS['REDIS'] = new \Redis();
			$GLOBALS['REDIS']->connect( $options['host'], $options['port'], $options['timeout'] );
		} catch( \RedisException $e ) {
			$GLOBALS['REDIS'] = null;
		}
		if( $GLOBALS['REDIS'] ) {
			if( isset( $options['auth'] ) ) {
				$GLOBALS['REDIS']->auth( $options['auth'] );
			}
			if( isset( $options['dbindex'] ) ) {
				$GLOBALS['REDIS']->select( $options['dbindex'] ); # session handler uses database 0, so we use 1 for app data
			}
		}
	}

	/**
	 * flushAll - Delete all keys from all redis databases. See https://github.com/phpredis/phpredis#flushall.
	 *
	 * @return void
	 */
	static function flush() : void {
		if( $GLOBALS['REDIS'] ) {
			$GLOBALS['REDIS']->flushAll();
		}
	}

	/**
	 * dbGetKeys - Get all keys from the given database.
	 *
	 * NOTE: This method will select the given database id, so you may have to select the correct db after calling this.
	 *       E.g. If your app uses database 1, and you are using database 0 for sessions, and then call this method to get keys for database 0, then you must select database 1 after calling this for your app to continue using database 1.
	 *		$keys = $GLOBALS['REDIS']->dbKeysGet( 0 );
	 *		$GLOBALS['REDIS']->select( 1 );
	 *
	 * @param int $dbNumber The database id
	 * @param string $pattern
	 *
	 * @return array of keys
	 */
	static function dbKeysGet( int $dbNumber, string $pattern = '*' ) : array {
		if( $GLOBALS['REDIS'] ) {
			$GLOBALS['REDIS']->select( $dbNumber );
			return $GLOBALS['REDIS']->keys( $pattern );
		} else {
			return [];
		}
	}

	/**
	 * dbKeysValuesGet - Get all keys and values from the given database.
	 *
	 * NOTE: Same note for dbGetKeys() applies here
	 *
	 * @param int $dbNumber The database id
	 * @param string $pattern
	 *
	 * @return array associative array of keys and values.
	 */
	static function dbKeysValuesGet( int $dbNumber, string $pattern = '*' ) : array {
		$output = [];
		if( $GLOBALS['REDIS'] ) {
			$GLOBALS['REDIS']->select( $dbNumber );
			$keys = $GLOBALS['REDIS']->keys( $pattern );
			foreach( $keys as $key ) {
				$output[$key] = $GLOBALS['REDIS']->get( $key);
			}
		}
		return $output;
	}

	/**
	 * lockAcquire - Implementation of the Redis Redlock algorithm https://redis.io/topics/distlock
	 * 	  This is the redis version of Mutual Exclusive (MutEx) Lock to ensure only one of multiple servers acquire a lock, so that a process, e.g. cron, only gets run once within the server group.
	 *
	 *	example usage:
	 *	$lockKey = 'cron1';
	 *	list( $lockAcquired, $lockValue ) = BonsaiCode\Redis::lockAcquire( $lockKey );
	 *	if( $lockAcquired ) {
	 *
	 *		... your processing here ...
	 *
	 *		BonsaiCode\Redis::lockRelease( $lockKey, $lockValue );
	 *	}
	 *
	 *
	 * @param string $key The given name of the lock to acquire.
	 * @param int $ttlSeconds Optional. The default timeout, in seconds, of the lock.  In case a process gets hangs or dies, this timeout will free up the lock so that it can be acquired again after this timeout period.
	 * @return array [ bool $status, string value ], status - true if lock acquired, false otherwise; value - is the value used for the lock which is need to release the lock
	 */

	static function lockAcquire( string $key, int $ttlSeconds = 90 ) : array {
		if( $GLOBALS['REDIS'] ) {
			# To minimize chance of collision with multiple web servers trying to acquire the lock at same time, we wait a random number of microseconds (up tp 1 second) before getting the lock.
			# 1 second = 1,000,000 microseconds
			$randomTenthOfSecond = random_int( 0, 999999 );
			#echo $randomTenthOfSecond;
			usleep( $randomTenthOfSecond );

			# Generate a value that is unique across all redis clients and lock requests.
			$value = php_uname( 'n' ) . ':' . microtime() . ':' . $randomTenthOfSecond;
			return [
				$GLOBALS['REDIS']->set( $key, $value, [ 'NX', 'EX' => $ttlSeconds ] ),
				$value
			];
		} else {
			return [ false, '' ];
		}
	}

	/**
	 * lockRelease - Release the lock acquired by lockAcquire().
	 *
	 * For more information see https://github.com/phpredis/phpredis#del-delete-unlink
	 *
	 * @param string $key The same $key used to acquire the lock via lockAcquire()
	 * @param string $value The value returned from lockAcquire() when acquiring the lock.
	 *
	 * @return int Number of keys deleted.
	 */
	static function lockRelease( string $key, string $value ) {
		$status = 0;
		if( $GLOBALS['REDIS'] ) {
			if( $GLOBALS['REDIS']->get( $key ) === $value ) {
				$status = $GLOBALS['REDIS']->del( $key );
			}
		}
		return $status;
	}

}