<?php
exit;
require_once $_SERVER['APP_CONFIG'];

header( 'Content-Type: text/plain' );

$lockKey = 'lock';

echo "Test 1\n";
list( $lockAcquired, $lockValue ) = BonsaiCode\Redis::lockAcquire( $lockKey );
if( $lockAcquired ) {
	echo "acquired lock\n\n";

	$GLOBALS['REDIS']->select( 1 );
	$allKeys = $GLOBALS['REDIS']->keys( '*' );
	echo "KEYS DB 1 - app "; print_r( $allKeys );

	sleep( 3 );

	BonsaiCode\Redis::lockRelease( $lockKey, $lockValue );
	echo "released lock\n\n";

	$GLOBALS['REDIS']->select( 1 );
	$allKeys = $GLOBALS['REDIS']->keys( '*' );
	echo "KEYS DB 1 - app "; print_r( $allKeys );

} else {
	echo "could not acquire lock\n\n";
}

# Try to reacquire the same lock, it should not allow it.

echo "Test 2\n";
list( $lockAcquired1, $lockValue1 ) = BonsaiCode\Redis::lockAcquire( $lockKey );
if( $lockAcquired1 ) {
	echo "acquired lock\n\n";
	list( $lockAcquired2, $lockValue2 ) = BonsaiCode\Redis::lockAcquire( $lockKey );
	if( $lockAcquired2 ) {
		echo "acquired lock again - BAD\n\n";
		RedisCluster::lockRelease( $lockKey, $lockValue2 );
	} else {
		echo "could not acquire lock again - GOOD\n\n";
	}
	BonsaiCode\Redis::lockRelease( $lockKey, $lockValue1 );
} else {
	echo "could not acquire lock1\n\n";
}

# Get lock and wait for TTL to expire, see if it goes away after 90 seconds

echo "Test 3\n";
list( $lockAcquired, $lockValue ) = BonsaiCode\Redis::lockAcquire( $lockKey );
if( $lockAcquired ) {
	echo "acquired lock. See if TTL works\n";
} else {
	echo "could not acquire lock\n\n";
}

echo "db1 Show keys: "; print_r( BonsaiCode\Redis::dbKeysGet( 1 ) );
#echo "db1 Show key & values: "; print_r( BonsaiCode\Redis::dbKeysValuesGet( 1 ) );

echo "db0 Show keys: "; print_r( BonsaiCode\Redis::dbKeysGet( 0 ) );
#echo "db0 Show key & values: "; print_r( BonsaiCode\Redis::dbKeysValuesGet( 0 ) );

# Select the application's redis database for normal operations

$GLOBALS['REDIS']->select( 1 );
